<?php
/*

   Copyright 2016 GILIA

   Author: GILIA

   docs.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
   Documenting ontology.

   to be published

   Try this command:

   @return A Web page.
 */

require_once '../../common/import_functions.php';
load("config.php", "../../config/");
load('translator.php', '../../wicom/translator/');
load('owldocument.php', '../../wicom/translator/documents/');
load('owllinkdocument.php', '../../wicom/translator/documents/');
load('crowd_uml.php','../../wicom/translator/strategies/');
load('owlbuilder.php', '../../wicom/translator/builders/');
load('htmlbuilder.php', '../../wicom/translator/builders/');
load('owllinkbuilder.php', '../../wicom/translator/builders/');
load('berardistrat.php','../../wicom/translator/strategies/');

load('documenter.php','../../common/');
load('widococonnector.php','../../wicom/reasoner/');


use Wicom\Translator\Translator;
use Wicom\Translator\Documents\OWLDocument;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Strategies\Berardi;
use Wicom\Translator\Builders\OWLBuilder;
use Wicom\Translator\Builders\OWLlinkBuilder;
use Wicom\Translator\Builders\HTMLBuilder;
use Wicom\Reasoner\WidocoConnector;

$documenter = 'widoco';
if (array_key_exists('documenter',$_REQUEST)){
    $documenter = $_REQUEST['documenter'];
}

if ( ! array_key_exists('json', $_POST)){
    echo "
There's no \"json\" parameter :-(
Use, for example:

    curl -d 'json={\"classes\": [{\"attrs\":[], \"methods\":[], \"name\": \"Hi World\"}]}' http://host.com/translator/crowd.php";
}else{

    switch ($documenter){
    case "widoco" :
        $strat = new UMLcrowd();
        $strat->change_min_maxTo_false();
        $res = new Translator($strat, new OWLBuilder());
        $owl2 = $res->to_owl2($_POST['json']);
        $doc = new Documenter($owl2, new WidocoConnector());
        echo $doc->generateDoc();
        break;
    default:
        die("Invalid Format!");
    }

    //print_r($res);
}

?>
