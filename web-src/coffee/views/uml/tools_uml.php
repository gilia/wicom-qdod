<?php
/*

   Copyright 2017 Giménez, Christian

   Author: Giménez, Christian

   tools_uml.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="navbar-nav">
  <div class="nav-item dropdown">
    <a role="button"
       class="navbar-brand crowd-header nav-link dropdown-toggle" href="#"
       id="uml-dropdown-btn" data-toggle="dropdown"
       aria-haspopup="true" aria-expanded="false">
      crowd v0.1
    </a>

    <div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
      <a class="dropdown-item" href="#" id="disclaimer">Disclaimer</a>
      <a class="dropdown-item" href="#" id="features">Features</a>
    </div>

  </div>

  <div class="nav-item dropdown">
    <a role="button" class="nav-link dropdown-toggle" href="#"
    id="uml-dropdown-btn" data-toggle="dropdown"
    aria-haspopup="true" aria-expanded="false">
      User
    </a>

    <div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
      <a class="dropdown-item" href="https://sites.google.com/a/fi.uncoma.edu.ar/german-braun/contact-me" target="_blank">
        I would link to have a <span class="badge badge-secondary">crowd</span> account
      </a>
      <a class="dropdown-item" href="#" id="log-in">
        Log-in/Log-out
      </a>
  </div>
  </div>

  <div class="nav-item dropdown">

	<a role="button" class="nav-link dropdown-toggle" href="#"
	   id="uml-dropdown-btn" data-toggle="dropdown"
	   aria-haspopup="true" aria-expanded="false">
	    Conceptual Model
	</a>

	<div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
	    <a class="dropdown-item" href="#" id="load-save">Load/Save</a>
      <a class="dropdown-item" href="#" id="namespaces">Namespaces</a>
      <a class="dropdown-item" href="#" id="owllink">Insert OWLlink axioms</a>

      <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" id="export-owl">Export OWL 2</a>

      <div class="dropdown-divider"></div>
<!--        <a class="dropdown-item" href="#" id="import-owl" style="cursor:not-allowed;opacity:0.5;">Import OWL 2</a> -->
        <a class="dropdown-item" href="#" id="import-owl">Import OWL 2 <b style="color:red">(beta)</b></a>

      <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" id="switch" style="cursor:not-allowed;opacity:0.5;">Switch Modelling Language</a>

      <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" id="documenter">Generate OWL 2 Docs</a>

      <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" id="import-json">Import JSON</a>
        <a class="dropdown-item" href="#" id="export-json">Export JSON</a>

      <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="#" id="clear-widget">Remove whole diagram</a>
	</div>
  </div>


  <div class="nav-item dropdown">

	<a role="button" class="nav-link dropdown-toggle" href="#"
	   id="uml-dropdown-btn" data-toggle="dropdown"
	   aria-haspopup="true" aria-expanded="false">
	    Reasoning
	</a>

	<div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
	    <a class="dropdown-item" href="#" id="menu-check-consistency">Start Reasoner</a>
      <a class="dropdown-item" href="#" id="reasoning-output">Reasoning Output</a>
	</div>
  </div>


    <div class="nav-item dropdown">
	     <a role="button" class="nav-link dropdown-toggle" href="#" id="uml-dropdown-btn" data-toggle="dropdown"
	         aria-haspopup="true" aria-expanded="false">
	          Editing
	     </a>

	     <div class="dropdown-menu" aria-labelledby="uml-dropdown-btn">
	        <a class="dropdown-item" href="#" id="add-class-uml">Add Class</a>
	        <a class="dropdown-item" href="#" id="add-assoc-uml" style="cursor:not-allowed;opacity:0.5;">Add Association</a>
          <a class="dropdown-item" href="#" id="add-gen-uml" style="cursor:not-allowed;opacity:0.5;">Add Generalisation</a>
	     </div>

    </div>

</div>
