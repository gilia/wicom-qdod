<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   importjson.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" id="namespaces_widget" tabindex="-1" role="dialog"
     aria-labelledby="namespaces_widget" aria-hidden="true">

    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title">Namespaces</h3>
                <button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
		<label>Ontology IRI prefix:</label>
		<div class="input-group">
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="ontoiri_prefix" value="" size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="ontoiri_value" value=""/>
        <input type="text" placeholder="OK"
 			   class="form-control"
 			   id="ontoiri_warn" value="" disabled="disabled"/>
		</div>

		<label>Prefixes</label>
		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp1_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv1_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv1_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp2_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv2_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv2_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp3_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv3_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv3_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp4_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv4_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv4_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp5_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv5_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv5_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp6_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv6_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv6_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp7_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv7_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv7_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp8_input" value=""  size="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv8_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv8_warn" value="" disabled="disabled"/>
		</div>

		<div class="input-group" >
		    <input type="text" placeholder="prefix"
			   class="form-control"
			   id="nsp9_input" value=""  maxsize="10"/>
		    <input type="text" placeholder="http://example.org#"
			   class="form-control"
			   id="nsv9_input" value="" />
         <input type="text" placeholder="OK"
  			   class="form-control"
  			   id="nsv9_warn" value="" disabled="disabled"/>
		</div>
    <div class="input-group" >
        <input type="text" placeholder="prefix"
         class="form-control"
         id="nsp10_input" value=""  size="10"/>
        <input type="text" placeholder="http://example.org#"
         class="form-control"
         id="nsv10_input" value="" />
         <input type="text" placeholder="OK"
           class="form-control"
           id="nsv10_warn" value="" disabled="disabled"/>
    </div>
            </div>
            <div class="modal-footer">
                <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary"
			    id="accept_namespaces_btn" disabled="disabled">
			Add
		    </button>
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">
                        Hide
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
