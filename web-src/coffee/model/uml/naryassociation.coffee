# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# A Link between two classes or more classes.
#
# This give support for two (using from() or to()) or
# more classes.
#
# @namespace model.uml
class NaryAssociation extends model.MyModel
    # @param classes {Array<Class>} An array of Class objects,
    #   the first class is the "from" and the second is the "to" class
    #   in a two-linked relation.
    constructor: (@classes, name = null, @position) ->
        if name?
            super(name)
        else
            super(NaryAssociation.get_new_name())

        @roles = []
        @associationClass = null

    # Return true if it uses a diamond as intersection
    has_diamond: () ->
        true

    # Following functions returns each one of the roles associated
    # "u" roles mean user defined roles and f = from/t = to.
    # "d" roles mean default roles and f = from/t = to. They take the class name.
    get_role: () ->
      return @roles

    add_roleTo_assoc: (role) ->
      @roles.push(role)

    # Set the roles. Roles are first citizen objects so that more resposabilities are assigned.
    #
    # @param [string] user_d_role is a user define role for the current link.
    # @param [string] class_r is the class associated to the current role. This name is also used as default role name.
    # @param [string] mult is the cardinality for the current role.
    set_roles: (user_d_role, class_r, mult, role) ->
      if user_d_role == undefined or user_d_role == ""
         role.set_role_URI(class_r.get_name().toLowerCase())
      else
        role.set_role_URI(user_d_role.toLowerCase())

      role.set_mult(mult)
      role.set_class(class_r)

      return role

    get_assoc_labels: () ->
      name = 'URI Assoc: '.concat @get_uri().get_fullname()
      prefix = 'Prefix: '.concat @get_uri().get_prefix_uri()
      url = 'URL: '.concat @get_uri().get_url_uri()
      assocClass = 'Assoc Class: '.concat ""

      if @associationClass != null
        assocClass = 'Assoc Class: '.concat @associationClass.get_uri().get_fullname()

      return [[name, prefix, url, assocClass],[]]


    add_class: (class_a) ->
      if class_a not in @classes
        @classes.push(class_a)

    has_role: (role) ->
      @roles.includes(role)

    has_role_to_class: (class_a) ->
      e_role = null

      @roles.forEach((elt, index, arr) ->
        if elt.get_class().get_fullname() == class_a.get_fullname()
          e_role = elt
        )

      return e_role

    remove_role: (role) ->
      @roles = @roles.filter( (elt, index, arr) ->
            elt != role
        )

    # @param class_from an instance of Class.
    set_from : (class_from) ->
        @classes[0] = class_from

    get_from : () ->
        return @classes[0]

    set_to : (class_to) ->
        @classes[1] = class_to

    get_to: () ->
        return @classes[1]

    get_classes: () ->
        return @classes

    set_association_class: (@associationClass) ->

    get_association_class: () ->
      return @associationClass

    # True if a two-linked relation. False otherwise.
    is_two_linked: () ->
        return @classes.length == 2

    # *Implement in the subclass if necesary.*
    #
    # @param parentclass {Class} The Class instance to check.
    # @return `true` if this is a generalization class and has the given parent instance. `false` otherwise.
    has_parent: (parentclass) ->
        return false

    # Check is n-ary association is with class
    #
    # @return true if association is with class. Otherwise, false.
    has_association_class:() ->
      return @associationClass != null


    to_json: () ->
        json = super()
        json.classes = $.map(@classes, (myclass) ->
            myclass.get_fullname()
        )
        length = @classes.length
        json.multiplicity = $.map(@roles, (role) ->
            if role.get_mult() == null
              return null
            else return role.get_mult()
        )

        json.roles = $.map(@roles, (role) ->
            role.get_fullname()
        )

        if @associationClass != null
          json.associated_class = @associationClass.to_json()

        if length > 2
          if @associationClass != null
            json.type = "n-ary association with class"
          else
            json.type = "n-ary association without class"
        else
          if @associationClass != null
            json.type = "association with class"
          else
            json.type = "association"
        if @joint?
          json.position = @joint[0].position()

        return json

    #
    # @see MyModel#create_joint
    create_joint: (factory, csstheme = null) ->
        if @joint == null
            @joint = []
            pos1 = graph.getCell(@classes[0].get_classid()).get("position")
            pos2 = graph.getCell(@classes[1].get_classid()).get("position")
            if @position == null
              @position =
                x:(pos1.x + pos2.x) / 2,
                y:(pos1.y + pos2.y) / 2
            if csstheme != null
                @joint.push(factory.create_nary_association(
                    @classes[0].get_classid(),
                    @classes[1],
                    null
                    csstheme.css_links,
                    @mult,
                    @roles,
                    @position))
            else
                @joint.push(factory.create_nary_association(
                    @classes[0].get_classid(),
                    @classes[1],
                    null,
                    null,
                    @mult,
                    @roles,
                    @position))

    # Compare the relevant elements of these links.
    #
    # @param other {Link}
    # @return {boolean}
    same_elts: (other) ->
        if !super(other)
            return false
        if @classes.length != other.classes.length
            return false

        # it must have the same order!
        all_same = true
        for index in [0...@classes.length]
            do (index) =>
                all_same = all_same && @classes[index].same_elts(other.classes[index])

        all_same = all_same && v == other.mult[k] for v,k in @mult

        all_same = all_same && v == other.roles[k] for v,k in @roles


        return all_same


NaryAssociation.get_new_name = () ->
    if NaryAssociation.name_number == undefined
       NaryAssociation.name_number = 0
    NaryAssociation.name_number = NaryAssociation.name_number + 1
    return "r" + NaryAssociation.name_number


exports.model.uml.NaryAssociation = NaryAssociation
