# interface.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# CreateClassView instance
exports = exports ? this

# {Diagram} = require './diagram'
# {Class} = require './mymodel'

# ----------------------------------------------------------------------

window.onload = () ->
    # Un poco de aliasing para acortar el código.
    #    uml = joint.shapes.uml;

    graph = new joint.dia.Graph
    paper = new joint.dia.Paper
        el: $('#container')
        width: 1600
        height: 1200
        overflow: scroll
        model: graph
        gridSize: 1
###
    paperSmall = new joint.dia.Paper
        el: $('#container-small')
        model: graph
        width: 1600 * 0.25
        height: 1200 * 0.25
        gridSize: 1
        interactive: false

    paperSmall.scale(0.25)


    minimapNavigatorPosition = {
      minX: 100
      minY: 687
      maxX: 249
      maxY: 770
    }

    $('#container-small').height(1200 * 0.25)
    $('#container-small').width(1600 * 0.25)

    # Set minimap navigator width, height
    containerWidthHeight = $('#container').width() # height, width both are set to same in css
    $('#minimap-navigator').width(containerWidthHeight * 0.25)
    $('#minimap-navigator').height(containerWidthHeight * 0.25)

    # Bind container scrolling
    $('#container').scroll( (e) ->
      $('#minimap-navigator').css('top', minimapNavigatorPosition.minY + e.target.scrollTop * 0.25)
      $('#minimap-navigator').css('left', minimapNavigatorPosition.minX + e.target.scrollLeft * 0.25)
    )

    # Bind minimap navigator drag
    dragFlag = false
    x = 0
    y = 0

    $('#minimap-navigator').mousedown( (e) ->
      dragFlag = true
      x = $(this).offset().left - e.clientX
      y = $(this).offset().top - e.clientY
    )

    $('#minimap-navigator').mouseup( () ->
      dragFlag = false
    )

    $('#container-small').mouseleave( () ->
      dragFlag = false
    )

    $('#minimap-navigator').mousemove( (e) ->
      if dragFlag
        newY = e.clientY + y
        newX = e.clientX + x

        if minimapNavigatorPosition.minY > newY
          newY = minimapNavigatorPosition.minY

        if minimapNavigatorPosition.minX > newX
          newX = minimapNavigatorPosition.minX

        if minimapNavigatorPosition.maxY < newY
          newY = minimapNavigatorPosition.maxY

        if minimapNavigatorPosition.maxX < newX
          newX = minimapNavigatorPosition.maxX

        $('#minimap-navigator').css('top', newY)
        $('#minimap-navigator').css('left', newX)

        $('#container').scrollLeft((newX - minimapNavigatorPosition.minX) / 0.25)
        $('#container').scrollTop((newY - minimapNavigatorPosition.minY) / 0.25)
     )
###

    # Export the graph for debugging.
    exports.graph = graph
    # Export the paper for debugging.
    exports.paper = paper

#    exports.paper = paperSmall


    guiinst = new gui.GUI graph, paper
    gui.set_current_instance guiinst

    # Export the guiinst for easy debugging.
    exports.guiinst = guiinst

    gui.eer.initialize graph, paper
    # gui.eer.set_current_instance eergui
    gui.uml.initialize graph, paper

    # We add available GUIs. The last will be the current one.
    guiinst.add_gui 'eer', gui.eer.iguieer
    #    gui.gui_instance.add_gui('uml', new gui.GUIUML())
    guiinst.add_gui 'uml', gui.uml.iguiuml

    # Interface

    # Events for the Paper

    # A Cell was clicked: select it.
    paper.on("cell:pointerclick",
        (cellView, evt, x, y) ->
            guiinst.on_cell_clicked(cellView, evt, x, y)
    )


    graphTreeLayout = new joint.layout.TreeLayout({
      graph: graph,
      parentGap: 100,
      siblingGap: 100
      })

    graphTreeLayout.layout()


    graphForceLayout = new joint.layout.ForceDirected({
      graph: graph,
      width: 600,
      height: 400,
      gravityCenter: { x: 300, y: 200 },
      charge: 180,
      linkDistance: 30
      })


#    graphGridLayout = new joint.layout.GridLayout.layout({
#      graph: graph,
#    })

#joint.layout.DirectedGraph.layout(graph,{nodeSep: 100, edgeSep: 200, rankDir: "BT"});

    exports.graphForceLayout = graphForceLayout

    exports.graphTreeLayout = graphTreeLayout

    #UML mode
#    newclass = new model.uml.Class('Person',["dni : String","firstname : String", "surname : String", "birthdate : Date"],[])
#    newclass1 = new model.uml.Class('Student',["id : String", "enrolldate : Date"],[])
#    newclass = new model.uml.Class('Person',[],[])
#    newclass1 = new model.uml.Class('Student',[],[])
#    console.log(newclass)
#    gui.gui_instance.current_gui.diagadapter.add_object_type(newclass)
#    gui.gui_instance.current_gui.diagadapter.add_object_type(newclass1)
