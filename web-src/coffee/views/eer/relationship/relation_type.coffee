exports = exports ? this
exports.views = exports.views ? this
exports.views.eer = exports.views.eer ? this
exports.views.eer.relationship = exports.views.eer.relationship ? this

RelationTypeView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_relation_type").html() )
        this.$el.html(template({classid: @classid}))

    events:
        'click a#rel_type_button' : 'new_relation'

    new_relation: (event) ->
        binary = $("#chk-binary").prop("checked")
        gui.gui_instance.hide_options()
        if binary
          type="binary"
        else
          type="nary"
        gui.gui_instance.set_editassociation_classid([@classid,binary,type])
        @hide()

    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x + 100,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    get_classid: () ->
        return @classid

    hide: () ->
        this.$el.hide()

    show: () ->
        this.$el.show()


)

exports.views.eer.relationship.RelationTypeView = RelationTypeView
