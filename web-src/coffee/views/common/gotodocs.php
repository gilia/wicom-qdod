<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   clock_wait.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>


<div class="modal fade" id="docs" tabindex="-1" role="dialog"
     aria-labelledby="docs" aria-hidden="true">

    <div class="modal-dialog">
	     <div class="modal-content">

	    <div class="modal-header">
        <h4>Ontology Documentation</h4>
      </div>

      <div class="alert alert-info">
        Link to Ontology Documentation
      </div>

	    <div class="modal-body">

        <center>
          <a id="link" href="#" target="_blank"> <strong>Follow me to discover crowd Ontology docs</strong> <a/>
        </center>

      </div>
      
      <div>
        Browse file <span class="badge badge-secondary">index-en.html</span> for launching main page
      </div>


	    <div class="modal-footer">
        <button type="button" class="btn btn-danger"
          data-dismiss="modal">
            Close
        </button>
	    </div>

	</div>
    </div>

</div>
