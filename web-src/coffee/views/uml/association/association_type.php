<?php
/*

Copyright 2018, Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA)

Author: Facultad de Informática, Universidad Nacional del Comahue

association_type.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


?>

<div class="assocOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="relationoptions_classid"  name="classid"  value="<%= classid %>" />

    <div class="btn-group">
	<button class="btn btn-secondary" type="button"
		title="Select Association Type"
		id="assoc_binary_btn">
	    Binary
	</button>
	<button type="button" class="btn btn-secondary"
		id="assoc_nary_btn" disabled="disabled">
	    N-ary
	</button>
  <button class="btn btn-danger" type="button"
     id="uml_close_assoctype_button">
       Close
  </button>
    </div>

</div>
