# login_widget.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @namespace login
# Widget for login.


LoginWidgetView = Backbone.View.extend(
    initialize: () ->
        @doing_login = true
        this.render()
        # We need to initialize the login-popup div because
        # jquery-mobile.js script is loaded before this script.
        #this.$el.hide()
        $("button#login_login_btn").on("click", this.do_login)
        $("button#logout_logout_btn").on("click", this.do_logout)
    render: () ->
        template = _.template( $("#template_loginwidget").html() )
        this.$el.html( template() )
        this.set_doing_login(@doing_login)

    hide: () ->
        $("#loginWidget").modal("hide")

    show: () ->
        this.set_doing_login(@doing_login)
        $("#loginWidget").modal("show")

    # events:
        # "click a#login_cancel_btn" : this.hide
        # "click a#login_login_btn" : "do_login"
    do_login: (event) ->
        login.lm_instance.hide_login()
        nl = login.lm_instance.new_login($("#login_username").val())
        nl.do_login($("#login_password").val())

    do_logout: (event) ->
        login.lm_instance.hide_login()
        login.lm_instance.current.do_logout()

    # Is this widget doing login or logout?
    #
    # Affects the next time it is called the show().
    #
    # @param doing_login {boolean} If true, the next time the dialog is shown it will display the login widgets.
    set_doing_login: (@doing_login) ->
        if @doing_login
            $("#logoutForm").hide()
            $("#loginForm").show()
        else
            $("#loginForm").hide()
            $("#logoutForm").show()

)


exports = exports ? this
exports.login = exports.login ? {}

# @namespace login
#
exports.login.LoginWidgetView = LoginWidgetView
