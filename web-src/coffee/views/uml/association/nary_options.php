<?php
/*

Copyright 2018, Grupo de Investigación en Lenguajes e Inteligencia Artificial (GILIA)

Author: Facultad de Informática, Universidad Nacional del Comahue

nary_options.php

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


?>

<div class="binaryOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden"
	   id="umlrelationoptions_classid"
	   name="classid"  value="<%= classid %>" />

    <div data-role="controlgroup" style="float: left">
	<form id="name-rel">
      <input  class="form-control"
		    placeholder="name"
		    type="text"
		    size="4" maxlength="10"
		    id="uml_nary_name" />
	    <input class="form-control"
		   placeholder="prefix"
		   type="text"
		   size="4" maxlength="10"
		   id="uml_nary_name_prefix" />
	    <input class="form-control"
		   placeholder="URL"
		   type="text"
		   size="4" maxlength="10"
		   id="uml_nary_name_url" />

      <div class="btn-group">
		      <button class="btn btn-primary" type="button"
			       id="nary_button">
		    Association
		    </button>
		      <button class="btn btn-secondary" type="button"
			       id="nary_class_button">
		           Association Class
		      </button>
          <button class="btn btn-danger" type="button"
			       id="uml_close_nary_button">
		           Close
		      </button>
      </div>
	</form>
</div>
</div>
