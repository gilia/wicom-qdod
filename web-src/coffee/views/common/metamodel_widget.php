<?php 
/* 

   Copyright 2018 Giménez, Christian
   
   Author: Giménez, Christian   

   metamodel_widget.php
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="modal fade" id="metamodel_widget" tabindex="-1" role="dialog"
     aria-labelledby="error_widget" aria-hidden="true">
    <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		<h1 class="modal-title">Metamodel</h1>
		<button type="button" class="close" data-dismiss="modal"
			aria-label="close">
		    <span aria-hidden="true">&times;</span>
		</button>
	    </div>

	    <div class="modal-body">
		<p>Select languaje</p>
		<div class="btn-group" role="group">
		    <button type="button" class="btn btn-secondary disabled"
			    id="metamodel_uml">
			UML
		    </button>
		    <button type="button" class="btn btn-secondary"
			    id="metamodel_eer">
			EER
		    </button>
		    <button type="button" class="btn btn-secondary disabled"
			    id="metamodel_orm">
			ORM
		    </button>
		</div>
	    </div>

	    <div class="modal-footer">
		<button type="button" class="btn btn-secondary"
			data-dismiss="modal">
		    Hide
		</button>
	    </div>
	</div>
    </div>
</div>
