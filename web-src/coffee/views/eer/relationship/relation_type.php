<div class="relOptions" style="visible:false, z-index:1, position:absolute">
    <input type="hidden" id="relationoptions_classid"  name="classid"  value="<%= classid %>" />
    <a class="ui-btn ui-corner-all ui-icon-arrow-u ui-btn-icon-notext" type="button" title="Select Association Type" id="rel_type_button">Relation</a>
    <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
      <input type="radio" id="chk-binary" name="rel" value="binary">
      <label for="chk-binary">Binary</label>
      <input type="radio" id="chk-nary" name="rel" value="nary">
      <label for="chk-nary">N-ary</label>
    </fieldset>
</div>
