# umlimporter.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.model = exports.model ? {}
exports.model.uml = exports.model.uml ? {}

# UMLImporter
#
# @namespace model.uml
class UMLImporter

    # @param diagram {UMLDiagram} The diagram where to import the objects.
    # @param json {string} The JSON string.
    # @param json {object} The parsed JSON string.
    constructor: (@diagram, json) ->
        if typeof(json) == "string"
            @_json = JSON.parse(json)
        else
            @_json = json
#        @_normalize()

    # Import all classes and links without clearing the diagram
    do_import: () ->
      gui.gui_instance.current_gui.diagadapter.reset_all()
      @import_namespaces()
      @import_classes()
      @import_links()

    # Normalize the JSON into a more usable format.
    #
    # Change attributes format for each classes:
    # From `{name: Namestr, datatype: Typestr}` into  a string "Name:Typestr"
    _normalize: () ->
        @_json.classes.forEach( (elt, index, arr) ->
            array = []
            if elt.attrs?
                attr = elt.attrs
                attr.forEach( (cv,index,attr) ->
                    att = "#{attr[index].name}:#{attr[index].datatype}"
                    array.push(att)
                    return array
                )
            elt.attrs = array
        )

    # Import namespaces and update all views
    import_namespaces: () ->
      @diagram.reset_ns()
      @diagram.update_ontologyIRI @_json.namespaces.ontologyIRI[0]["value"], @_json.namespaces.ontologyIRI[0]["prefix"]

      @_json.namespaces.defaultIRIs.forEach(
        (elt, index, arr) ->
          @diagram.add_default_ns elt.value, elt.prefix
      this)

      @_json.namespaces.IRIs.forEach(
        (elt, index, arr) ->
          @diagram.add_custom_ns elt.value, elt.prefix
      this)

      gui.gui_instance.widgets.refresh_ns()

    # Normalize the JSON provided and import all its classes.
    import_classes: () ->
        @_json.classes.forEach(
            (elt, index, arr) ->
                elt_with_attr = elt.attrs
                elt.attrs = []

                if elt.position?
                  if elt.position.x != -10000 and elt.position.y != -10000
                    @diagram.add_class(elt)
                else
                  @diagram.add_class(elt)

                if elt_with_attr?
                    elt_with_attr.forEach( (attr, index, arr) ->
                      name_at = attr.name
                      type = attr.datatype
                      class_a = @diagram.find_class_by_name elt.name
                      @diagram.add_attribute class_a, name_at, type
                    this)
            this)

    import_links: () ->
        @_json.links.forEach(
            (elt, index, arr) ->
                console.log elt
                if elt.type is "association" or elt.type is "association with class" or
                  elt.type is "n-ary association with class" or elt.type is "n-ary association without class"
                    console.log("association " + elt.classes[0] + " - " + elt.classes[1])
                    class_a = @diagram.find_class_by_name(elt.classes[0])

                    console.log class_a

                    i = 1
                    class_b = []

                    while i < elt.classes.length
                      class_b.push(@diagram.find_class_by_name(elt.classes[i]).get_classid())
                      i = i + 1

                    console.log "Class B", class_b
                    console.log "REL NAME", elt.name

                    if elt.associated_class?
                        console.log "ES UNA RELACI CON CLASS"
                        @diagram.add_association_class(
                            class_a.get_classid(),
                            class_b,
                            elt.name,
                            elt.multiplicity,
                            elt.roles,elt.position)
                    else
                        @diagram.add_association(
                            class_a.get_classid(),
                            class_b,
                            elt.name,
                            elt.multiplicity,
                            elt.roles,elt.position)
                if elt.type is "generalization"
                    class_parent = @diagram.find_class_by_name(elt.parent)
                    classes_children = elt.classes.map(
                        (childname) ->
                            @diagram.find_class_by_name(childname)
                    this)
                    disjoint = elt.constraint.includes("disjoint")
                    covering = elt.constraint.includes("covering")
                    @diagram.add_generalization(
                        class_parent,
                        classes_children,
                        disjoint, covering,
                        elt.name,elt.position)

        this)


exports.model.uml.UMLImporter = UMLImporter
