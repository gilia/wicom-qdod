<?php
/*

   Copyright 2018 Giménez, Christian

   Author: Giménez, Christian

   reasoning_widget.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>

<div class="container modal fade" id="uml_reasoning_widget" tabindex="-1" role="dialog"
     aria-labelledby="uml_reasoning_widget" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title">Reasoning Settings</h4>
                <button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="alert alert-warning">
              <strong>Warning!</strong> Maybe you need to check OWLlink axioms before reasoning!
            </div>

            <div class="modal-body">
		<form>
		    <div class="form-group">
			Formal Encoding:
			<select class="custom-select" id="uml-reasoning-strat">
			    <option value="berardi" disabled="disabled">Berardi, Calvanese, De Giacomo - 2005</option>
			    <option selected="1" value="crowd">crowd</option>
			</select>
		    </div>
		    <div class="form-group">
			Off-the-shelf Reasoner:
			<select class="custom-select" id="uml-reasoning-program">
			    <option selected="1" value="Racer">Racer</option>
			    <option value="Konclude" disabled="disabled">Konclude</option>
			</select>
		    </div>
		</form>
            </div>

            <div class="alert alert-info">
              <strong>Info!</strong>
              After reasoning you can see results using <span class="badge badge-secondary">Reasoning -> Reasoning Output</span>
            </div>

            <div class="modal-footer">
                <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary"
			    id="uml-run-btn">
			Check
		    </button>
                    <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">
                        Hide
                    </button>
                </div>
            </div>

        </div>
    </div>
</div>
