<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   globals.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

 // model.root.uripatternbar = () -> ///^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\/([a-zA-Z0-9])+)$///i
 // model.root.uripatternhash = () -> ///^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\#([a-zA-Z0-9])+)$///i
 // model.root.prefixpatternname = () -> ///([a-zA-Z0-9])+\:([a-zA-Z0-9])+///i

 /**
 Reg Exp for URI patterns and prefixes
 */
 $GLOBALS["uripatternbar"]  = '/^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\/([a-zA-Z0-9])+)$/i';

 $GLOBALS["uripatternhash"]  = '/^((http|https):\/\/([a-zA-Z0-9\/\.\-\_])+\#([a-zA-Z0-9])+)$/i';

 $GLOBALS["prefixpatternname"]  = '/([a-zA-Z0-9])+\:([a-zA-Z0-9])+/i';

?>
