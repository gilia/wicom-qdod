<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   tools_navbar.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

?>
<div data-role="navbar">
    <label>Reasoning</label>
    <select data-mini="true" data-inline="true" data-native-menu="false" id="strategy_select">
	     <option value="crowd" selected="true">crowd</option>
	     <option value="berardi">Berardi/Calvanese</option>
    </select>

    <select data-mini="true" data-inline="true" data-native-menu="false" id="reasoner">
      <option value="Konclude">Konclude</option>
      <option value="Racer"  selected="true">Racer</option>
    </select></br>

    <label>Translating</label>
    <select data-mini="true" data-inline="true" data-native-menu="false" id="format_select">
        <option value="owlxml" selected="true">OWL 2 OWL/XML</option>
        <option value="owlfunctional">OWL 2 Functional</option>
        <option value="rdfxml">OWL 2 RDF/XML</option>
        <option value="rdfturtle">OWL 2 Turtle</option>
        <option value="rdfmanchester">OWL 2 Manchester</option>
        <option value="html">DL (Human Readable)</option>
        <option value="owllink">OWLlink</option>
    </select>

    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="translate_button1">Translate</a>

    <label>Ontology Namespaces</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="namespaces_open_dialog">Set Namespaces</a>

    <label>Import OWL 2 Ontology</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="importowl_open_dialog">Import OWL 2 (OWL/XML)</a>

    <label>Go through KF-Metamodel</label>
    <a class="ui-btn ui-icon-plus ui-btn-icon-left ui-corner-all" type="button" id="meta_erd_button">ERD</a>
    <a class="ui-btn ui-icon-plus ui-btn-icon-left ui-corner-all" type="button" id="meta_orm_button">ORM</a>
    <a class="ui-btn ui-icon-plus ui-btn-icon-left ui-corner-all ui-state-disabled" type="button" id="meta_uml_button">UML</a>

    <!--label>New Class</label>
    <input data-mini="true" placeholder="ClassName" type="text" id="crearclase_input"/>
    <a class="ui-btn ui-icon-plus ui-btn-icon-left ui-corner-all" type="button" id="umlcrearclase_button">New</a> -->

    <label>Insert OWLlink data</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="insertowllink_button">Insert OWLlink</a>
    <label>Reset All</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="resetall_button">Reset All</a>
    <label>Import JSON</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="importjson_open_dialog">Import JSON</a>
    <label>Export JSON</label>
    <a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all" type="button" id="exportjson_open_dialog">Export JSON</a>
    <hr/>
    <p>Profile</p>
    <div data-role="controlgroup" data-type="horizontal">
	<a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all ui-btn-inline ui-mini" type="button" id="savejson">Save</a>
	<a class="ui-btn ui-icon-edit ui-btn-icon-left ui-corner-all ui-btn-inline ui-mini" type="button" id="savejson">Load</a>
    </div>
</div>
