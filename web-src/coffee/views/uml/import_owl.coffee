# import_json.coffee --
# Copyright (C) 2018 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? {}


# View widget for the "Import JSON".
ImportOWLView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template( $("#template_importowl").html() )
        this.$el.html template({})

    events:
        "click button#importowl_importbtn" : "do_import"
        "click button#importowl_importbtn_cancel" : "hide"

    hide: () ->
        this.$el.children(0).modal("hide")

    # Show this widget.
    #
    # @param pos {object} Optional. The position, for example: `{x: 10, y: 20}`
    # @param callback_fnc [function] Optional. A callback function without parameters. If not setted, then use the default.
    show: () ->
        this.$el.children(0).modal("show")

    do_import: () ->
        owlstr = $("#paste_owl").val()
        #console.log(owlstr)
        gui.gui_instance.current_gui.import_owl owlstr
)

exports.views.uml.ImportOWLView = ImportOWLView
