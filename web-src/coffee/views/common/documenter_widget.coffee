# reasoning_widget.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


DocumenterWidget = Backbone.View.extend
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template $("#template_documenter_widget").html()
        this.$el.html template({})

    events:
        'click button#doc-run-btn': 'generate_docs'

    get_documenter: () ->
        this.$el.find("select#documenter-tool").val()

    generate_docs: () ->
        documenter = this.get_documenter()
        gui.gui_instance.documenting documenter
        @hide()

    show: () ->
      $("#documenter_widget").modal("show")

    hide: () ->
      $("#documenter_widget").modal("hide")


exports.views.common.DocumenterWidget = DocumenterWidget
