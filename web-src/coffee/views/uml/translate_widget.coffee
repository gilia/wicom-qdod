# translate_widget.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? {}

TranslateWidget = Backbone.View.extend
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template $("#template_translate_widget").html()
        this.$el.html template({})

    events:
        "click button#uml-translate-btn": "translate"

    get_syntax: () ->
        this.$el.find("select#uml-output-type").val()

    get_method: () ->
        this.$el.find("select#uml-translation-method").val()

    translate: () ->
        strat = this.get_method()
        syntax = this.get_syntax()
        
        gui.gui_instance.current_gui.translate_formal strat, syntax

    set_text: (data) ->
        syntax = this.get_syntax()        
        if syntax is 'html'
            this.$el.find('#html-output').html data
            this.$el.find('#uml-owllink-source').hide()
        else
            this.$el.find('#html-output').hide()
            this.$el.find('#uml-owllink-source').val data


    hide: () ->
        this.$el.children(0).modal("hide")

    show: () ->
        this.$el.children(0).modal("show")

exports.views.uml.TranslateWidget = TranslateWidget


