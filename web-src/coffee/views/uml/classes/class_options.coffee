# class_options.coffee --
# Copyright (C) 2016 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.views = exports.views ? this
exports.views.uml = exports.views.uml ? this
exports.views.uml.classes = exports.views.uml.classes ? this

# @mixin
# @namespace views
ClassOptionsView = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.hide()

    render: () ->
        template = _.template( $("#template_classoptions").html() )
        this.$el.html(template({classid: @classid}))

    events:
        "click button#umldeleteclass_button" : "delete_class",
        "click button#umleditclass_button" : "edit_class",
        "click button#umleditattrs_button" : "edit_attributes"
        "click button#umlassoc_button" : "edit_assoc",
        "click button#umlgen_button" : "edit_gen",
        "click button#metadata_button" : "ns",

    ##
    # Set the classid of the Joint Model associated to this EditClass
    # instance, then set the position of the template to where is the
    # class Joint Model.
    set_classid: (@classid) ->
        viewpos = graph.getCell(@classid).findView(paper).getBBox()

        this.$el.css(
            top: viewpos.y + 50,
            left: viewpos.x,
            position: 'absolute',
            'z-index': 1
            )
        this.$el.show()

    ##
    # Return the ClassID of the Joint.Model element associated to
    # this EditClass instance.
    get_classid: () ->
        return @classid

    delete_class: (event) ->
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.diagadapter.delete_class(@classid)

    edit_class: (event) ->
        gui.uml.iumlwidgets.toolbar.set_feedback("Editing class")
        gui.uml.iumlwidgets.hide_options()
        gui.uml.iumlwidgets.editclass.load_prefixes(@classid)
        gui.uml.iumlwidgets.set_editclass_classid(@classid)
        gui.state_inst.selectionstate_inst.reset_click()
        this.hide()

    edit_attributes: (event) ->
        gui.uml.iumlwidgets.toolbar.set_feedback("Editing attributes")
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.uml.iumlwidgets.editattrs.load_prefixes()
        gui.gui_instance.current_gui.widgets.set_editattribute_classid(@classid)
        gui.state_inst.selectionstate_inst.reset_click()
        this.hide()

    edit_gen: (event) ->
        gui.uml.iumlwidgets.toolbar.set_feedback("Setting generalisation")
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.widgets.set_editgeneralization_classid(
            @classid)
        gui.state_inst.selectionstate_inst.reset_click()
        @hide()

    edit_assoc: (event) ->
        gui.uml.iumlwidgets.toolbar.set_feedback("Setting association")
        gui.gui_instance.current_gui.widgets.hide_options()
        gui.gui_instance.current_gui.widgets.set_editassociation_classid(
            @classid)
        gui.state_inst.selectionstate_inst.reset_click()
        @hide()

    ns: (event) ->
      gui.uml.iumlwidgets.hide_options()
      info = gui.gui_instance.current_gui.diagadapter.show_class_info(@classid)
      gui.uml.iumlwidgets.show_info info
      gui.state_inst.selectionstate_inst.reset_click()

    hide: () ->
        this.$el.hide()
)


exports.views.uml.classes.ClassOptionsView = ClassOptionsView
