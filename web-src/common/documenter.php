<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   documenter.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

load("translator.php", "../wicom/translator/");
load("owlbuilder.php", "../wicom/translator/builders/");
load("crowd_uml.php", "../wicom/translator/strategies/");
load("berardistrat.php", "../wicom/translator/strategies/");

load("runner.php", "../wicom/reasoner/");
load("widococonnector.php", "../wicom/reasoner/");
load("koncludeconnector.php", "../wicom/reasoner/");


use Wicom\Translator\Translator;
use Wicom\Translator\Strategies\UMLcrowd;
use Wicom\Translator\Strategies\Berardi;

use Wicom\Reasoner\Runner;
use Wicom\Reasoner\RacerConnector;
use Wicom\Reasoner\WidocoConnector;
use Wicom\Reasoner\KoncludeConnector;

use Wicom\Translator\Builders\UMLJSONBuilder;
use Wicom\Translator\Decoder;


/**
   Documenter module

 */
class Documenter{
    protected $documenter = null;
    protected $owl2 = null;

    function __construct($owl2, $documenter){
        $this->owl2 = $owl2;
        $this->documenter = $documenter;
    }

    /**
       @param json A String.
       @return an XML OWLlink String.
     */
    function generateDoc(){
        $runner = new Runner($this->documenter);
        $runner->run($this->owl2);
        return $runner->get_last_answer()[0];
    }

}
