# tools_uml.coffee --
# Copyright (C) 2017 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.views = exports.views ? {}
exports.views.uml = exports.views.uml ? this


# UML Toolbar.
#
# Acoplable toolbar for the most common primitives creation.
ToolsUMLView = Backbone.View.extend(
    initialize: () ->
        @render()
        @classnum = 0

    render: () ->
        template = _.template( $("#template_tools_uml").html(), {} )
        this.$el.html(template)

    events:
        'click a#menu-check-consistency': 'check_consistency'
        'click a#reasoning-output': 'reasoning_output'
        'click a#menu-translate-uml': 'translate'
        'click a#add-class-uml': 'umlclass_pressed'
        'click a#add-assoc-uml': 'umlassoc_pressed'
        'click a#add-gen-uml': 'umlisa_pressed'
        'click a#log-in': 'log_in'
        'click a#load-save': 'save_load'
        'click a#namespaces': 'namespaces'
        'click a#owllink': 'owllink'
        'click a#import-json': 'import_json'
        'click a#export-json': 'export_json'
        'click a#export-owl': 'export_owl'
        'click a#import-owl': 'import_owl'
        'click a#clear-widget': 'remove_whole'
#        'click a#switch': 'switch'
        'click a#documenter': 'documenter'
        'click a#disclaimer': 'disclaimer'
        'click a#features': 'features'


    umlclass_pressed: () ->
        @classnum += 1
        gui.gui_instance.current_gui.diagadapter.add_object_type(
            name: "Class" + @classnum
        )

    umlassoc_pressed: () ->

    umlisa_pressed: () ->

    translate: () ->
        gui.uml.iumlwidgets.show_translation()

    check_consistency: () ->
        gui.uml.iumlwidgets.show_reasoning()

    reasoning_output: () ->
      gui.gui_instance.widgets.reasoningdetailswidget.show()

    log_in: () ->
      login.lm_instance.show_login()

    save_load: () ->
      $("#saveloadjsonwidget").modal("show")

    namespaces: () ->
      gui.gui_instance.widgets.namespacewidget.show()

    owllink: () ->
      gui.gui_instance.widgets.owllinkinsert.show()

    import_json: () ->
      gui.gui_instance.widgets.importjsonwidget.show()

    export_json: () ->
      gui.gui_instance.widgets.exportjsonwidget.show()

    export_owl: () ->
      gui.uml.iumlwidgets.translatewidget.show()

    import_owl: () ->
      gui.uml.iumlwidgets.importowlwidget.show()

    remove_whole: () ->
      gui.gui_instance.widgets.clearwidget.show()

    switch: () ->
#      gui.gui_instance.widgets.namespacewidget.show()

    documenter: () ->
      gui.gui_instance.widgets.documenterwidget.show()


    disclaimer: () ->
      $("#disclaimer_widget").modal("show")

    features: () ->
      $("#news_widget").modal("show")


    # Write a status message or a short feedback for the user.
    set_feedback: (str) ->
      $("#feedback_widget").val str

    # Show and enable the widget.
    enable: () ->
        this.$el.show()

    # Hide and disable the widget.
    disable: () ->
        this.$el.hide()


    $("#zoom_25").on "click", -> gui.gui_instance.zoom 0.25, 0.25
    $("#zoom_50").on "click", -> gui.gui_instance.zoom 0.50, 0.50
    $("#zoom_75").on "click", -> gui.gui_instance.zoom 0.75, 0.75
    $("#zoom_normal").on "click", -> gui.gui_instance.zoom 1, 1
    $("#zoom_125").on "click", -> gui.gui_instance.zoom 1.25, 1.25
    $("#fit").on "click", -> gui.gui_instance.fit()

)

exports.views.uml.ToolsUML = ToolsUMLView
