# widgets.coffee --
# Copyright (C) 2018 Giménez, Christian

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.gui = exports.gui ? {}
exports.gui.eer = exports.gui.eer ? {}


# @namespace gui.eer
#
# Widgets used in EER GUI only.
class EERWidgets extends gui.WidgetMgr
    constructor: () ->
        super()
        @eerview = new views.eer.entity.CreateEntityView
            el: $("#eercreateentity_placeholder")
        @toolbar = new views.eer.ToolsEERView
            el: $("#eer_toolbar_placeholder")

        @editclass = new views.eer.entity.EditEntityView
            el: $("#eereditentity_placeholder")
        @classoptions = new views.eer.entity.EntityOptionsView
            el: $("#eerentityoptions_placeholder")
        @relationoptions = new views.eer.relationship.RelationOptionsView
            el: $("#eerrelationoptions_placeholder")
        @isaoptions = new views.eer.isa.IsaOptionsView
            el: $("#eerisaoptions_placeholder")
        @attroptions = new views.eer.attributes.AttrOptionsView
            el: $("#eerattroptions_placeholder")

    enable: () ->
        # @eerview.enable()
        @toolbar.enable()
        # @classoptions.enable()
        # @relationoptions.enable()
        # @editclass.enable()
        # @isaoptions.enable()
        # @attroptions.enable()

    disable: () ->
        # @eerview.disable()
        @toolbar.disable()
        # @classoptions.disable()
        # @relationoptions.disable()
        # @editclass.disable()
        # @isaoptions.disable()
        # @attroptions.disable()


    # Set the class Id of the class options GUI.
    set_options_classid: (model_id) ->
        @relationoptions.set_classid(model_id)
        @classoptions.set_classid(model_id)
        @isaoptions.set_classid(model_id)
        @attroptions.set_attrid(model_id)
        @classoptions.show()

    # Hide the class options GUI.
    hide_options: () ->
        @classoptions.hide()
        @relationoptions.hide()
        @editclass.hide()
        @isaoptions.hide()
        @attroptions.hide()

# Unique instance of UMLWidgets
#
# @namespace gui.eer
exports.gui.eer.ieerwidgets = new EERWidgets()

exports.gui.eer.EERWidgets = EERWidgets
