<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   news_widget.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="modal fade" id="news_widget" tabindex="-1" role="dialog"
     aria-labelledby="error_widget" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		      <h4 class="modal-title" style="color:blue">NEWS! feature crowd release</h4>
	    </div>

	    <div class="modal-body">
        <p><b style="color:green">UML graphical support.</b> Classes, Attributes, Binary associations with/without association
          classes, Covering and/or Disjoint Generalisations and End Roles. Each one of them is associated to a URI</p>
        <p> <b style="color:green">Namespaces definition.</b> Prefixes and URLs. Any added primitive in crowd takes a default URI. Users can
        define their own URIs and use prefixes when adding an element to the diagram. If prefixes added do not exist,
        crowd also puts its default</p>
        <p><b style="color:green">Export OWL 2 (OWL/XML)</b>. We are working hard to add support for more standard OWL syntaxes</p>
        <p><b style="color:green">Export OWLlink.</b> Ontology can be exported to feed other reasoners</p>
        <p> <b style="color:green">Reasoners:</b> Racer 2.0 and SPARQL-DL. Other reasoners will be added to crowd in order to test them and
        compare different tools.</p>
        <p><b style="color:green">Insert OWLlink expressions.</b> This capability allows to append to the graphical ontology
        any other more expressive axioms which will be to sent to reasoning module</p>
        <p><b style="color:green">Graphical-Logical Integration.</b> crowd is based on a theoretical framework for
        extracting as many implicit axioms as possible in order to be graphically depicted. In particular, crowd
        reveals implicit subsumptions and cardinalities greater than 1 in addition to inconsistent classes and propeties.</p>
        <p><b style="color:green">Ontology Documentation.</b> crowd integrates documentation platforms such as Widoco for the ontology
          generated. A link will be provided to download it and use it wherever you want.</p>
        <p><b style="color:green">User login and model management.</b> User should ask to crowd developers for a crowd account in order to save
          their models. Please follow this link <a href="https://sites.google.com/a/fi.uncoma.edu.ar/german-braun/contact-me">
            <strong>i would like having a crowd account and testing it</strong> </a></p>
        <p> <b style="color:green">Import/Export support.</b> In order to keep diagrams, users can export their diagrams to be imported
          back into crowd.
        <p> <b style="color:red">EER graphical support. TBA</b>
        <p> <b style="color:red">ORM graphical support. TBA</b>
        <p> <b style="color:red">Metamodelling. TBA</b>. crowd metamodel is based on a novel KF metamodel capturing common features of them.
          So we expect to convert, transform and approximate between this graphical syntaxes by keeping a core representation.
        <p> <b style="color:red">OBDA. TBA</b>. We are working hard to release a new version with OBDA support. This capability will include
          automapping and ontop to query SQL databases
        <p> <b style="color:red">Reasoning support. TBA</b>. Konclude
        <p> <b style="color:red">UML Verbalisation. TBA</b>
        <p> <b style="color:red">OWL import. TBA</b>. This feature is one of the most important and novel for us. It will allow to import
          OWL 2 ontologies into a graphical web tool to and using the UML graphical language. Non-graphic primitive and expressions
          will be also kept into the tool. Users can reason over them.
		  </div>

	    <div class="modal-footer">
		      <button type="button" class="btn btn-primary" data-dismiss="modal">
		          Hide
		      </button>
	    </div>
	</div>
  </div>
</div>
