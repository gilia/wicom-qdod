<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   disclaimer_widget.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>

<div class="modal fade" id="disclaimer_widget" tabindex="-1" role="dialog"
     aria-labelledby="error_widget" aria-hidden="true">
  <div class="modal-dialog" role="document">
	<div class="modal-content">
	    <div class="modal-header">
		      <h4 class="modal-title" style="color:blue"><span class="badge badge-secondary">crowd</span> Disclaimer</h4>
	    </div>

      <div class="alert alert-warning">
        Data generated in <span class="badge badge-secondary">crowd</span> will be hosted
        at <a href="https://www.uncoma.edu.ar/" target="_black">Universidad Nacional del Comahue</a> and
        <a href="https://www.uns.edu.ar/" target="_black">Universidad Nacional del Sur</a>.
        We are not responsible for any loss or damage. Data will not be used for any commercial issue.
      </div>

	    <div class="modal-footer">
		      <button type="button" class="btn btn-primary" data-dismiss="modal">
		          Hide
		      </button>
	    </div>
	</div>
  </div>
</div>
