<?php
/*

   Copyright 2018 GILIA

   Author: GILIA

   clock_waitfordocs.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="modal fade" id="clock_waitfordocs" tabindex="-1" role="dialog"
     aria-labelledby="clock_waitfordocs" aria-hidden="true">

    <div class="modal-dialog">
	     <div class="modal-content">

	    <div class="modal-header">
		<h5 class="modal-title">Documenting...Please Wait for link!</h5>
	    </div>

	    <div class="modal-body">

        <center>
          <img id="clock_img" src="imgs/clock.gif" alt="Documenting...Please Wait for link!"/>
        </center>
      </div>

	</div>
    </div>

</div>
