<?php
/*

   Copyright 2016 Giménez, Christian

   Author: Giménez, Christian

   classoptions.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


?>
<div class="attrOptions" data-role="controlgroup"
     style="visible:false, z-index:1, position:absolute" >
    <input type="hidden" id="attroptions_attrid"
	   name="attrid" value="<%= attrid %>" />
    <button class="btn btn-primary btn-sm" type="button" id="eereditattr_button">
	Edit
    </button>
    <button class="btn btn-danger btn-sm" type="button" id="eerdeleteattr_button">
	Delete
    </button>
    <button class="btn btn-primary btn-sm" type="button" title="Link with Class" id="eerlinkwithclass_button">
        Link
    </button>
</div>
