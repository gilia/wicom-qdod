exports = exports ? this
exports.views = exports.views ? {}
exports.views.common = exports.views.common ? {}


ReasoningDetailsWidget = Backbone.View.extend(
    initialize: () ->
        this.render()
        this.$el.show()

    render: () ->
        template = _.template( $("#template_reasoning").html() )
        this.$el.html(template({}))


    set_kb: (satisfiable_kb) ->
      $("#reasoner_kb").val("")
      $("#reasoner_kb").val(satisfiable_kb)

    set_reasoner_output: (reasoner_output) ->
      $("#reasoner_output").val("")
      $("#reasoner_output").val(reasoner_output)

    set_sat: (sat_c, sat_op, sat_dp) ->
      text = sat_c.concat sat_op
      html_sat = text.concat sat_dp
      $("#reasoner_sat").val("")
      $("#reasoner_sat").val(html_sat)

    set_unsat : (unsat_c, unsat_op, unsat_dp) ->
      text = unsat_c.concat unsat_op
      html_unsat = text.concat unsat_dp
      $("#reasoner_unsat").val("")
      $("#reasoner_unsat").val(html_unsat)

    show: () ->
        this.$el.children(0).modal("show")

    hide: () ->
        this.$el.children(0).modal("hide")
    )


exports.views.common.ReasoningDetailsWidget = ReasoningDetailsWidget
