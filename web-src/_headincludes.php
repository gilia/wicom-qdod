<?php
include './config/config.php'
?>
<meta charset="utf-8"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no" />
<meta name="author" content="Giménez, Christian"/>
<?php
if ($environment == 'production'){
?>
    <link rel="stylesheet" href="./css/joint.min.css" />
    <link rel="stylesheet" href="./css/bootstrap.min.css">
<?php
}else{
?>
    <link rel="stylesheet" href="./css/joint.css" />
    <link rel="stylesheet" href="./css/bootstrap.css">
<?php
}

// include "_modelinclude.php";
?>

