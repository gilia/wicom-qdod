<?php
/*

   Copyright 2018 Giménez, Christian

   Author: Giménez, Christian

   reasoning_widget.php

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
?>
<div class="modal fade" id="documenter_widget" tabindex="-1" role="dialog"
     aria-labelledby="documenter_widget" aria-hidden="true">

    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h3 class="modal-title">Generate Documentation</h3>
                <button type="button" class="close" data-dismiss="modal"
			      aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

  <div class="modal-body">
		<form>
		    <div class="form-group">
			Documentation Platform:
			<select class="custom-select" id="documenter-tool">
			    <option value="widoco" selected="1">WIDOCO</option>
			</select>
		    </div>
		</form>
  </div>

      <div class="modal-footer">
          <div class="btn-group" role="group">
		          <button type="button" class="btn btn-primary" id="doc-run-btn">
			             Generate
		          </button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Hide
              </button>
          </div>
      </div>

  </div>
</div>
</div>
